package com.saul.sendcardnfc.adapters;

import android.view.View;

/**
 * Created by saul on 5/1/18.
 */

public
interface OnItemClickListenerInterface {
    public  void onClick(View view, int position);
}
