package com.saul.sendcardnfc.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.saul.sendcardnfc.fragments.MyCardFragment;
import com.saul.sendcardnfc.fragments.ReCardFragment;

/**
 * Created by saul on 5/11/18.
 */

public
class CarteFragmentAdapter extends FragmentPagerAdapter {
    private String[] tabTile = new String []{"Card envoyes","card recus"};
    private Context context;
    public CarteFragmentAdapter (FragmentManager fm, Context context){
        super(fm);
        this.context =context;
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position==0){
            return new MyCardFragment();
        }
        else if (position==1){return new ReCardFragment();
        }
        else   return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTile[position];
    }
}



