package com.saul.sendcardnfc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.models.CarteVisite;

import java.util.List;

/**
 * Created by saul on 5/1/18.
 */

public
class CarteAdapter  extends RecyclerView.Adapter<CarteAdapter.ViewHolder> {
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvNom;
        TextView  tvTitre;
        TextView company_name;
        TextView tvDate;
       ImageView logo;
        public ViewHolder(final View view){
            super(view);
            tvNom=(TextView) view.findViewById(R.id.tvNom);
            tvTitre=(TextView) view.findViewById(R.id.tvTitre);
            company_name=(TextView) view.findViewById(R.id.company_name);

            tvDate=(TextView) view.findViewById(R.id.tvDate);
            logo=(ImageView) view.findViewById(R.id.logo);
         //  view.setOnClickListener(this);


            view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Triggers click upwards to the adapter on click
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(view, position);
                    }
                }
            }
        });
        }
    }


    private List<CarteVisite> cartes;
    // Store the context for easy access
    private Context mContext;

    // Pass in the contact array into the constructor
    public CarteAdapter(Context context, List<CarteVisite> _cartes) {
        cartes = _cartes;
        mContext = context;
    }

    // Easy access to the context object in the recyclerview
    private Context getContext() {
        return mContext;
    }


    @Override
    public CarteAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.item_card_recycleview, parent, false);
        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(CarteAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        CarteVisite vcarte = cartes.get(position);
        TextView tvNom=viewHolder.tvNom;
        // Set item views based on your views and data model
        TextView tvTitre = viewHolder.tvTitre;
        TextView company_name = viewHolder.company_name;
        TextView tvDate=viewHolder.tvDate;
        ImageView logo=viewHolder.logo;
        tvNom.setText(vcarte.getName().toString()+" "+vcarte.getPrenom().toString());
        company_name.setText(vcarte.getCompany_name().toString());
//        tvDate.setText(vcarte.getDateAt().toString());
        tvTitre.setText(vcarte.getTitre().toString());
        logo.setImageBitmap(decodeFromBase64ToBitmap(vcarte.getImage().toString()));


      /*  try {
            Glide.with(getContext())
                    .load(decodeFromBase64ToBitmap(vcarte.getImage().toString()))
                    //.override(500, 500)
                    .bitmapTransform(new RoundedCornersTransformation(getContext(), 30, 10))
                    .bitmapTransform(new CropCircleTransformation(getContext()))
                    .into(logo);
        }   catch (Exception e)

            {
                Log.e(TAG, e.getMessage());
            }*/




    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return cartes.size();
    }


    private
    Bitmap decodeFromBase64ToBitmap(String encodedImage)

    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
