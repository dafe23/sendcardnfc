package com.saul.sendcardnfc.datalayer;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by saul on 5/1/18.
 */
@Database(name = MyCardBase.NAME, version = MyCardBase.VERSION)
public
class MyCardBase {
    public static final String NAME = "CardDatabase";

    public static final int VERSION = 1;
}
