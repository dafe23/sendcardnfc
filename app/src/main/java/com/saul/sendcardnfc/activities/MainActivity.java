package com.saul.sendcardnfc.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.adapters.CarteAdapter;
import com.saul.sendcardnfc.adapters.CarteFragmentAdapter;
import com.saul.sendcardnfc.fragments.CardFragment;
import com.saul.sendcardnfc.fragments.SliderFragment;
import com.saul.sendcardnfc.fragments.ViewFragment;
import com.saul.sendcardnfc.models.CarteVisite;
import com.saul.sendcardnfc.utils.EndlessRecyclerViewScrollListener;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public
class MainActivity extends AppCompatActivity implements ViewFragment.ParseCardListener,NfcAdapter.CreateNdefMessageCallback,NfcAdapter.OnNdefPushCompleteCallback {

    ArrayList<CarteVisite> cartes;
    ProgressDialog pd;
    CarteAdapter adapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    List<CarteVisite> listCartes;
    Toolbar toolbar;
    CarteVisite maCarte;
    NfcAdapter mNfcAdapter;
    private static final int MESSAGE_SENT = 1;
    private String cardsend;
   Fragment fragment;
    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FlowManager.init(new FlowConfig.Builder(this).build());
        setContentView(R.layout.activity_main);
        fragment = (Fragment) new SliderFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .commit();

      //  toolbar = (Toolbar) findViewById(R.id.toolbar);
     //   setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
          //      showCreateDialog();
                launchComposeView();
             //   Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
             //           .setAction("Action", null).show();
            }
        });

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new CarteFragmentAdapter(getSupportFragmentManager(),this));
        TabLayout tabLayout =(TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        listCartes=  CarteVisite.AllCarteVisite();

        // Check for available NFC Adapter
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {

            Toast.makeText(this, "NFC is not available on this device.", Toast.LENGTH_SHORT).show();

        }else{
            // Register callback to set NDEF message
            mNfcAdapter.setNdefPushMessageCallback(this, this);
            // Register callback to listen for message-sent success
            mNfcAdapter.setOnNdefPushCompleteCallback(this, this);

        }
    }

    @Override
    public
    void onFinishParseCard(CarteVisite _data) {
        maCarte=_data;
        Toast.makeText(this, maCarte.getCompany_name() + " was clicked!", Toast.LENGTH_SHORT).show();
      //  }
        fragment = CardFragment.newInstance(maCarte);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .commit();

    }


    @Override
    public
    NdefMessage createNdefMessage(NfcEvent event) {
        Time time = new Time();
        time.setToNow();
        maCarte.setMy(true);
        Gson objectjson = new Gson();
        cardsend= objectjson.toJson(maCarte);

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
               maCarte.save();
            }
        });




        NdefMessage msg = new NdefMessage(new NdefRecord[] { createMimeRecord(
                        "application/com.saul.sendcardnfc.activities.mainactivity", cardsend.getBytes())

                });

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });



        return msg;
    }

    public NdefRecord createMimeRecord(String mimeType, byte[] payload) {
        byte[] mimeBytes = mimeType.getBytes(Charset.forName("US-ASCII"));
        NdefRecord mimeRecord = new NdefRecord(
                NdefRecord.TNF_MIME_MEDIA, mimeBytes, new byte[0], payload);
        return mimeRecord;
    }


    public void launchComposeView() {
     Intent i = new Intent(MainActivity.this, FormCarteActivity.class);
        startActivity(i);
    }
    private void showCreateDialog() {
        FragmentManager fm = getSupportFragmentManager();
        CreateCardDialog createCardDialog = CreateCardDialog.newInstance("Some Title");
        createCardDialog.show(fm, "fragment_edit_name");
    }
    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex=image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }



    /**
     * Implementation for the OnNdefPushCompleteCallback interface
     */
    @Override
    public void onNdefPushComplete(NfcEvent arg0) {
         mHandler.obtainMessage(MESSAGE_SENT).sendToTarget();
    }

    /** This handler receives a message from onNdefPushComplete */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_SENT:
                    Toast.makeText(getApplicationContext(), "Message sent!", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
           if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
           setIntent(intent);
    }
    void processIntent(Intent intent) {
        Toast.makeText(getApplicationContext(), "Message resevwa ui!", Toast.LENGTH_LONG).show();
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
             NdefMessage msg = (NdefMessage) rawMsgs[0];
       Gson _json = new Gson();
        String message = new String(msg.getRecords()[0].getPayload());
      ///  Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        CarteVisite card = _json.fromJson(message,CarteVisite.class);
        card.setMy(false);
        card.save();

    }


}

