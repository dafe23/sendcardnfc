package com.saul.sendcardnfc.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.models.CarteVisite;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public
class FormCarteActivity extends AppCompatActivity implements View.OnClickListener {




    // To retrieve object in second Activity
  //  getIntent().getSerializableExtra("MyClass");

    // Image loading result to pass to startActivityForResult method.
    private static int LOAD_IMAGE_RESULTS = 1;

    private ImageView image;// ImageView

    private Button btOk;
    private AutoCompleteTextView name;
    private AutoCompleteTextView prenom;
    private AutoCompleteTextView company_name;
    private AutoCompleteTextView cell_number;
    private AutoCompleteTextView work_number;
    private AutoCompleteTextView adresse;
    private AutoCompleteTextView mail;
    private AutoCompleteTextView website;
    private AutoCompleteTextView titre;
    CarteVisite carteedit;

    private String type_card ="Simple";
    public String _image="";
    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_carte);
        image = (ImageView)findViewById(R.id.image);
        btOk = (Button) findViewById(R.id.btOk);
        name = (AutoCompleteTextView) findViewById(R.id.name);
        prenom = (AutoCompleteTextView) findViewById(R.id.prenom);
        company_name = (AutoCompleteTextView)findViewById(R.id.company_name);
        cell_number = (AutoCompleteTextView) findViewById(R.id.cell_number);
        work_number = (AutoCompleteTextView) findViewById(R.id.work_number);
        adresse = (AutoCompleteTextView) findViewById(R.id.adresse);
        mail = (AutoCompleteTextView) findViewById(R.id.mail);
        website = (AutoCompleteTextView) findViewById(R.id.website);
        titre = (AutoCompleteTextView) findViewById(R.id.titre);
        Spinner spinner = (Spinner) findViewById(R.id.type_card);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.type_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
//        spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        try{

            Intent i = getIntent();
            carteedit = (CarteVisite)i.getSerializableExtra("Carte_Visite");
            company_name.setText(carteedit.getCompany_name());
         //   image = (ImageView)findViewById(R.id.image);
            name.setText(carteedit.getName());
            prenom.setText(carteedit.getPrenom());
            cell_number.setText(carteedit.getCell_number());
            work_number.setText(carteedit.getWork_number());
            adresse.setText(carteedit.getAdresse());
            mail.setText(carteedit.getMail());
            website.setText(carteedit.getWebsite());
            titre.setText(carteedit.getTitre());
            image.setImageBitmap(decodeFromBase64ToBitmap(carteedit.getImage().toString()));
        }catch (Exception e){

        }

        // Set button's onClick listener object.
        image.setOnClickListener(this);

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(carteedit!=null){
                    carteedit.setName(name.getText().toString());
                    carteedit.setPrenom(prenom.getText().toString());
                    carteedit.setCompany_name(company_name.getText().toString());
                    carteedit.setCell_number(cell_number.getText().toString());
                    carteedit.setWork_number(work_number.getText().toString());
                    carteedit.setAdresse(adresse.getText().toString());
                    carteedit.setMail(mail.getText().toString());
                    carteedit.setWebsite(website.getText().toString());
                    carteedit.setTitre( titre.getText().toString());
                    carteedit.setImage(_image);
                    carteedit.update();
                }else {

                    CarteVisite _queryCl = new CarteVisite(
                            name.getText().toString(),
                            prenom.getText().toString(),
                            company_name.getText().toString(),
                            cell_number.getText().toString(),
                            work_number.getText().toString(),
                            adresse.getText().toString(),
                            mail.getText().toString(),
                            website.getText().toString(),
                            type_card,
                            true, "dafevrun@gmail.com",
                            titre.getText().toString(),
                            _image);
                    _queryCl.save();
                }
                Intent i = new Intent(FormCarteActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public
    View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         if (requestCode == LOAD_IMAGE_RESULTS && resultCode == RESULT_OK && data != null) {
            Uri pickedImage = data.getData();
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            image.setImageBitmap(BitmapFactory.decodeFile(imagePath));
            _image= convertToBase64(imagePath);
            cursor.close();
        }
    }

    @Override
    public void onClick(View v) {
        // Create the Intent for Image Gallery.
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start new activity with the LOAD_IMAGE_RESULTS to handle back the results when image is picked from the Image Gallery.
        startActivityForResult(i, LOAD_IMAGE_RESULTS);
    }
    private Bitmap decodeFromBase64ToBitmap(String encodedImage)
    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
    private String convertToBase64(String imagePath)
    {
        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        return encodedImage;
    }



}
