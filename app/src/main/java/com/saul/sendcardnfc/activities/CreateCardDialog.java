package com.saul.sendcardnfc.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.models.CarteVisite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by saul on 5/1/18.
 */

public
class CreateCardDialog  extends DialogFragment {
    //  private EditText mSort;
    private Button btOk;
    private AutoCompleteTextView name;
    private AutoCompleteTextView prenom;
    private AutoCompleteTextView company_name;
    private AutoCompleteTextView cell_number;
    private AutoCompleteTextView work_number;
    private AutoCompleteTextView adresse;
    private AutoCompleteTextView mail;
    private AutoCompleteTextView website;
    private AutoCompleteTextView titre;
    public CreateCardDialog() {
        // Required empty public constructor
    }

    public static CreateCardDialog newInstance(String title) {
        CreateCardDialog fragment = new CreateCardDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }
    public interface EditCreateDialogListener {
        void onFinishEditDialog(CarteVisite _data);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        btOk = (Button) view.findViewById(R.id.btOk);

        name = (AutoCompleteTextView) view.findViewById(R.id.name);
        prenom = (AutoCompleteTextView) view.findViewById(R.id.prenom);
        company_name = (AutoCompleteTextView) view.findViewById(R.id.company_name);
        cell_number = (AutoCompleteTextView) view.findViewById(R.id.cell_number);
       work_number = (AutoCompleteTextView) view.findViewById(R.id.work_number);
        adresse = (AutoCompleteTextView) view.findViewById(R.id.adresse);
        mail = (AutoCompleteTextView) view.findViewById(R.id.mail);
        website = (AutoCompleteTextView) view.findViewById(R.id.website);

        titre = (AutoCompleteTextView) view.findViewById(R.id.titre);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        //   mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditCreateDialogListener listener = (EditCreateDialogListener) getActivity();
                CarteVisite _queryCl=new  CarteVisite(name.getText().toString(),
                       prenom.getText().toString(),
                        company_name.getText().toString(),
                    cell_number.getText().toString(),
                  work_number.getText().toString(),
               adresse.getText().toString(),
                mail.getText().toString(),
               website.getText().toString(),
              "",
              true, "dafevrun@gmail.com",
                        titre.getText().toString(),"");

                    //    CarteVisite _queryCl=new CarteVisite(bgDate.getText().toString(),textSearch,arr);
             listener.onFinishEditDialog(_queryCl);
                // Close the dialog and return back to the parent activity
                dismiss();
                //   return true;
            }
        });


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialogcreatecard, container);
    }


}
