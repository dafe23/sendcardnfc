package com.saul.sendcardnfc.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Method;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.saul.sendcardnfc.datalayer.MyCardBase;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saul on 5/1/18.
 */
@Table(database = MyCardBase.class)

public class CarteVisite extends BaseModel  implements Serializable {
    @PrimaryKey(autoincrement = true)

    @Column
    private int id;
    @Column
    private String name;
    @Column
    private String prenom;
    @Column
    private String company_name;
    @Column
    private String cell_number;
    @Column
    private String work_number;
    @Column
    private String adresse;
    @Column
    private String mail;
    @Column
    private String website;
    @Column
    private String type;
    @Column
    private boolean isMy;
    @Column
    private String createdAt;
    @Column
    private String dateAt;
    @Column
    private String titre;

    @Column
    private String image;

    public
    String getImage() {
        return image;
    }

    public
    void setImage(String image) {
        this.image = image;
    }

    public
    String getTitre() {
        return titre;
    }

    public
    void setTitre(String titre) {
        this.titre = titre;
    }

    public
    CarteVisite() {
    }

    public
    CarteVisite(String name, String prenom, String company_name, String cell_number, String work_number, String adresse, String mail, String website, String type, boolean isMy, String createdAt,String titre,String image) {
        this.name = name;
        this.prenom = prenom;
        this.company_name = company_name;
        this.cell_number = cell_number;
        this.work_number = work_number;
        this.adresse = adresse;
        this.mail = mail;
        this.website = website;
        this.type = type;
        this.isMy = isMy;
        this.createdAt = createdAt;
        this.titre=titre;
        this.image=image;
    }

    public
    int getId() {
        return id;
    }

    public
    void setId(int id) {
        this.id = id;
    }

    public
    String getName() {
        return name;
    }

    public
    void setName(String name) {
        this.name = name;
    }

    public
    String getPrenom() {
        return prenom;
    }

    public
    void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public
    String getCompany_name() {
        return company_name;
    }

    public
    void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public
    String getCell_number() {
        return cell_number;
    }

    public
    void setCell_number(String cell_number) {
        this.cell_number = cell_number;
    }

    public
    String getWork_number() {
        return work_number;
    }

    public
    void setWork_number(String work_number) {
        this.work_number = work_number;
    }

    public
    String getAdresse() {
        return adresse;
    }

    public
    void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public
    String getMail() {
        return mail;
    }

    public
    void setMail(String mail) {
        this.mail = mail;
    }

    public
    String getWebsite() {
        return website;
    }

    public
    void setWebsite(String website) {
        this.website = website;
    }

    public
    String getType() {
        return type;
    }

    public
    void setType(String type) {
        this.type = type;
    }

    public
    boolean isMy() {
        return isMy;
    }

    public
    void setMy(boolean my) {
        isMy = my;
    }

    public
    String getCreatedAt() {
        return createdAt;
    }

    public
    void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public
    String getDateAt() {
        return dateAt;
    }

    public
    void setDateAt(String dateAt) {
        this.dateAt = dateAt;
    }

    public static   List<CarteVisite> AllCarteVisite() {
        return new Select().from(CarteVisite.class).where(CarteVisite_Table.isMy.eq(true)).orderBy(CarteVisite_Table.id, false).limit(300).queryList();
    }
    public static   List<CarteVisite> AllRecCarteVisite() {
        return new Select().from(CarteVisite.class).where(CarteVisite_Table.isMy.eq(false)).orderBy(CarteVisite_Table.id, false).limit(300).queryList();
    }




}
