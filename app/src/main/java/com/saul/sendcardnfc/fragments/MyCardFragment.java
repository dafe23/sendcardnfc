package com.saul.sendcardnfc.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.adapters.CarteAdapter;
import com.saul.sendcardnfc.models.CarteVisite;
import com.saul.sendcardnfc.utils.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public
class MyCardFragment extends ViewFragment {
    ProgressDialog pd;
    List<CarteVisite> listCartes;
    public static MyCardFragment newInstance() {
        MyCardFragment fragment = new MyCardFragment();
        Bundle bundle = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            populateUserCarte();
        }catch (Exception e) {

        }


    }

    private void populateUserCarte() {
        listCartes=  CarteVisite.AllCarteVisite();
        addItems(listCartes);
    }


}
