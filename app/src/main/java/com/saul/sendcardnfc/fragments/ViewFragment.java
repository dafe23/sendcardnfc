package com.saul.sendcardnfc.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.adapters.CarteAdapter;
import com.saul.sendcardnfc.adapters.OnItemClickListenerInterface;
import com.saul.sendcardnfc.models.CarteVisite;

import java.util.ArrayList;
import java.util.List;

public
class ViewFragment extends Fragment {
    ArrayList<CarteVisite> cartes;
    ProgressDialog pd;
    CarteAdapter adapter;
    public
    ViewFragment() {
        // Required empty public constructor
    }
    public interface ParseCardListener {
        void onFinishParseCard(CarteVisite _data);
    }


    public static
    ViewFragment newInstance() {
        ViewFragment fragment = new ViewFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public
    View onCreateView(LayoutInflater inflater, ViewGroup container,
                      Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_view, container, false);
        RecyclerView rvCartes = (RecyclerView) view.findViewById(R.id.rvCarte);
//        adapter.notifyDataSetChanged();
        rvCartes.setAdapter(adapter);
      //  rvCartes.setLayoutManager(new LinearLayoutManager(getActivity()));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        rvCartes.addItemDecoration( new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        rvCartes.setLayoutManager(gridLayoutManager);
        return view;


    }

    public void addItems(List<CarteVisite> listCartes){
        cartes = new ArrayList<>();
        cartes.addAll(listCartes);
        adapter = new CarteAdapter( getActivity(), cartes);
        adapter.setOnItemClickListener(new CarteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CarteVisite carte = cartes.get(position);
                ParseCardListener listener = (ParseCardListener) getActivity();
                listener.onFinishParseCard(carte);
             //   Toast.makeText(getActivity(), carte.getCompany_name() + " was clicked!", Toast.LENGTH_SHORT).show();
            }
        });
      //  adapter.notifyItemInserted(tweets.size()-1);

    }

}
