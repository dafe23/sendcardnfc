package com.saul.sendcardnfc.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.activities.FormCarteActivity;
import com.saul.sendcardnfc.models.CarteVisite;

/**
 * Created by saul on 5/11/18.
 */

public
class CardFragment extends Fragment {
    private static final String CARTE_VISITE = "carte_visite";
    TextView name;
    TextView  prenom;
    TextView company_name;
    TextView titre;
    TextView mail;
    TextView  cell_number;
    TextView adresse;
    TextView website;
    private ImageView home;// ImageView

    private ImageView edit;// ImageView

    Fragment fragment;
    ImageView logo;
    private CarteVisite carteVisite;
    public CardFragment() {
        // Required empty public constructor
    }
    public static CardFragment newInstance(CarteVisite carte) {
        CardFragment fragment = new CardFragment();
        Bundle args = new Bundle();
        args.putSerializable(CARTE_VISITE, carte);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            carteVisite = (CarteVisite) getArguments().getSerializable(CARTE_VISITE);

        }
    }
    @Nullable
    @Override
    public
    View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.item_card, container, false);
        name=(TextView) view.findViewById(R.id.name);
        prenom=(TextView) view.findViewById(R.id.prenom);
        company_name=(TextView) view.findViewById(R.id.company_name);

        titre=(TextView) view.findViewById(R.id.titre);

        mail=(TextView) view.findViewById(R.id.mail);
        cell_number=(TextView) view.findViewById(R.id.cell_number);
        adresse=(TextView) view.findViewById(R.id.adresse);

        website=(TextView) view.findViewById(R.id.website);


        logo=(ImageView) view.findViewById(R.id.logo);

        home=(ImageView) view.findViewById(R.id.home);
        edit=(ImageView) view.findViewById(R.id.edit);

        name.setText(carteVisite.getName().toString());
        company_name.setText(carteVisite.getCompany_name().toString());
        mail.setText(carteVisite.getMail().toString());
        titre.setText(carteVisite.getTitre().toString());

        cell_number.setText(carteVisite.getCell_number().toString());
        adresse.setText(carteVisite.getAdresse().toString());
        website.setText(carteVisite.getWebsite().toString());
        prenom.setText(carteVisite.getPrenom().toString());


        logo.setImageBitmap(decodeFromBase64ToBitmap(carteVisite.getImage().toString()));


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = SliderFragment.newInstance();
               getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayout, fragment)
                        .commit();
            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(carteVisite.isMy()){
                    Intent intent = new Intent(getActivity(), FormCarteActivity.class);
                    Bundle bundle = new Bundle();
                    //   bundle.putParcelable("currentCard", card);
                    intent.putExtra("Carte_Visite", carteVisite);
                    startActivity(intent);
                }else {
                    Toast.makeText(getActivity(), "Vous ne pouvez pas editer cette carte", Toast.LENGTH_SHORT).show();
                }

            }
        });




        return view;
    }
    Bitmap decodeFromBase64ToBitmap(String encodedImage)

    {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
    @Override
    public
    void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}



