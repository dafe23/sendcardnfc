package com.saul.sendcardnfc.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saul.sendcardnfc.R;
import com.saul.sendcardnfc.adapters.CarteAdapter;
import com.saul.sendcardnfc.models.CarteVisite;
import com.saul.sendcardnfc.utils.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.List;

public
class ReCardFragment extends ViewFragment {


        ArrayList<CarteVisite> cartes;
        ProgressDialog pd;
        CarteAdapter adapter;
        private EndlessRecyclerViewScrollListener scrollListener;
        List<CarteVisite> listCartes;

    public
    ReCardFragment() {

    }
    public static
    ReCardFragment newInstance() {
        ReCardFragment fragment = new ReCardFragment();
        Bundle args = new Bundle();
            return fragment;
    }

    @Override
    public
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          populateUserCarte();

    }
    private void populateUserCarte() {
        listCartes=  CarteVisite.AllRecCarteVisite();
        addItems(listCartes);
    }


}
